package sample;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import java.util.ArrayList;
import java.util.List;

public class Controller {

    @FXML
    private JFXTextField amount;

    @FXML
    private JFXTextArea result;

    @FXML
    private JFXButton button;

    private List<String> list;

    @FXML
    void sorting(ActionEvent event) {
        check();
        getList();
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size() - i - 1; j++) {
                if (Integer.parseInt(list.get(j)) < Integer.parseInt(list.get(j + 1))) {
                    String i1 = list.get(j);
                    list.set(j,list.get(j+1));
                    list.set(j+1,i1);
                }
            }
        }
        output();
    }

    private void check() {
        if(Integer.parseInt(amount.getText())>31||Integer.parseInt(amount.getText())<0){

        }
    }

    private void output() {
        for (String aList : list) {
            result.setText(result.getText() + "\n" + aList);
        }

    }

    private void getList() {
        list= new ArrayList<String>();
        for (int i = 0; i <= Integer.parseInt(amount.getText()); i++) {
            list.add(i, String.valueOf((int) (Math.random() * 100)));
        }
    }

}

