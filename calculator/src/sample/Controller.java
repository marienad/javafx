package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    public Button point;

    @FXML
    public Button min;
    @FXML
    public Button zeros;
    @FXML
    public Button zero;
    @FXML
    public Button nine;
    @FXML
    public Button plus;
    @FXML
    public Button minus;
    @FXML
    public Button mul;
    @FXML
    public Button divide;
    @FXML
    public Button one;
    @FXML
    public Button two;
    @FXML
    public Button three;
    @FXML
    public Button delete;
    @FXML
    public Button four;
    @FXML
    public Button five;
    @FXML
    public Button six;
    @FXML
    public Button seven;
    @FXML
    public Button eight;

    @FXML
    private TextField result;

    @FXML
    private Button clear;

    @FXML
    private Button equals;

    @FXML
    void clear(ActionEvent event) {
        result.clear();
    }

    @FXML
    void equals(ActionEvent event) {
        if (isCorrect()) {
            String equation = String.valueOf(result.getText());
            String[] arrSplit = equation.split(" ");
            if(arrSplit[0].equals(".")||arrSplit[2].equals(".")){
                printError();
                return;
            }
            double number1 = Double.parseDouble(arrSplit[0]);
            double number2 = Double.parseDouble(arrSplit[2]);
            switch (arrSplit[1]) {
                case ("+"):
                    result.setText(String.valueOf(number1 + number2));
                    break;
                case ("-"):
                    result.setText(String.valueOf(number1 - number2));
                    break;
                case ("*"):
                    result.setText(String.valueOf(number1 * number2));
                    break;
                case ("/"):
                    if (number2 == 0) {
                        printError();
                    } else {
                        result.setText(String.valueOf(number1 / number2));
                    }
                    break;
            }
        }
    }

    private void printError() {
        result.setText("Ошибка");
    }

    @FXML
    private boolean isCorrect() {
        if (!result.getText().contains(" ") || result.getText().indexOf(" ") == result.getText().length() - 3) {
            printError();
            return false;
        }
        return true;
    }

    @FXML
    void addNumberToTextField(ActionEvent event) {
        checkTextField();
        Button button = (Button) event.getSource();
        result.setText(result.getText() + "" + button.getEllipsisString());
    }

    @FXML
    private void checkTextField() {
        if (result.getText().equals("Ошибка") || result.getText().equals("Infinity")) {
            result.clear();
        }
    }

    @FXML
    public void addSignToTextField(ActionEvent actionEvent) {
        checkTextField();
        if (result.getText().isEmpty()) {
            printError();
        } else {
            if (!(result.getText().contains("+") || result.getText().contains(" - ") || result.getText().contains("*") || result.getText().contains("/"))) {
                Button button = (Button) actionEvent.getSource();
                result.setText(result.getText() + " " + button.getEllipsisString() + " ");
            } else {
                printError();
            }
        }
    }

    @FXML
    public void addPointToTextField(ActionEvent actionEvent) {
        checkTextField();
        if (result.getText().indexOf(".", result.getText().indexOf(" ")) == -1) {
            Button button = (Button) actionEvent.getSource();
            result.setText(result.getText() + "" + button.getEllipsisString());
        } else {
            printError();
        }
    }

    @FXML
    public void delete(ActionEvent actionEvent) {
        checkTextField();
        if (!result.getText().isEmpty()) {
            result.setText(result.getText().trim());
            result.setText(result.getText().substring(0, result.getText().length() - 1));
            result.setText(result.getText().trim());
            if (result.getText().matches("-?\\d+ [-+/*]")) {
                result.setText(result.getText() + " ");
            }
        }
    }

    @FXML
    public void addZerosToTextField(ActionEvent actionEvent) {
        checkTextField();
        if (result.getText().isEmpty()) {
            printError();
        } else {
            result.setText(result.getText() + "00");
        }
    }

    @FXML
    public void setNegative(ActionEvent actionEvent) {
        checkTextField();
        if (result.getText().isEmpty() || result.getText().matches("-?\\d+ . ")) {
            result.setText(result.getText() + "-");
        } else {
            if (!result.getText().contains(" ")) {
                result.setText(String.valueOf(Double.parseDouble(result.getText()) * (-1)));
            } else {
                result.setText(result.getText().substring(0, result.getText().indexOf(" ") + 3) + String.valueOf(Double.parseDouble(result.getText().substring(result.getText().indexOf(" ") + 2)) * (-1)));
            }
        }
    }
}
