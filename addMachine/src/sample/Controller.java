package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class Controller {
    @FXML
    TextField result;
    @FXML
    TextField firstOperand;
    @FXML
    TextField secondOperand;

    public void add(ActionEvent actionEvent) {
        if (firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            return;
        }
        int first = Integer.parseInt(firstOperand.getText());
        int second = Integer.parseInt(secondOperand.getText());
        int sum = first + second;
        result.setText(String.valueOf(sum));
    }

    public void clear(ActionEvent actionEvent) {
        firstOperand.clear();
        secondOperand.clear();
        result.clear();
    }
}
