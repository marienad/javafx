package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {

    @FXML
    private Label firstResult;

    @FXML
    private Label secondResult;

    @FXML
    void plusFiveFirst(ActionEvent event) {
        plus(firstResult, 5);
    }

    @FXML
    void plusFiveSecond(ActionEvent event) {
        plus(secondResult, 5);
    }

    @FXML
    void plusThreeFirst(ActionEvent event) {
        plus(firstResult, 3);
    }

    @FXML
    void plusThreeSecond(ActionEvent event) {
        plus(secondResult, 3);
    }

    @FXML
    void plusTwoFirst(ActionEvent event) {
        plus(firstResult, 2);
    }

    @FXML
    void plusTwoSecond(ActionEvent event) {
        plus(secondResult, 3);
    }

    private void plus(Label Result, int i) {
        Result.setText(String.valueOf(Integer.parseInt(Result.getText()) + i));
    }
}
