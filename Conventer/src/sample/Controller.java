package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

public class Controller {

    @FXML
    private TextField kilometres;

    @FXML
    private RadioButton m;

    @FXML
    private ToggleGroup unit;

    @FXML
    private RadioButton sm;

    @FXML
    private RadioButton mm;

    @FXML
    private Button button;

    @FXML
    private TextArea result;

    @FXML
    void getKilometres(ActionEvent event) {
        if (m.isSelected()){
            result.setText(String.valueOf(Integer.parseInt(kilometres.getText())*1000));
        }
        if (sm.isSelected()){
            result.setText(String.valueOf(Integer.parseInt(kilometres.getText())*100000));
        }
        if (mm.isSelected()){
            result.setText(String.valueOf(Integer.parseInt(kilometres.getText())*1000000));
        }
    }
}
