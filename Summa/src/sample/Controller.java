package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private Label result;

    @FXML
    private TextField x;

    @FXML
    private TextField n;

    @FXML
    private Button button;

    @FXML
    void count(ActionEvent event) {
        double v = Double.parseDouble(x.getText());
        double s = 0;
        double a = 1;
        for (int i = 1; i <= Integer.parseInt(n.getText()); i++) {
            a = a * v / i;
            s = s + a;
        }
        result.setText(String.valueOf(s));
    }
}