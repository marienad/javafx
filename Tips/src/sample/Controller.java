package sample;

import com.jfoenix.controls.*;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.util.HashMap;
import java.util.Map;

public class Controller {
    @FXML
    public MaterialDesignIconView close;
    @FXML
    public AnchorPane window;
    @FXML
    public JFXButton settingsButton;
    @FXML
    public MaterialDesignIconView minimize;
    @FXML
    private AnchorPane main;

    @FXML
    private JFXTextField checkAmount;

    @FXML
    private JFXComboBox<String> valueBox;

    @FXML
    private JFXRadioButton zero;

    @FXML
    private ToggleGroup percent;

    @FXML
    private JFXRadioButton three;

    @FXML
    private JFXRadioButton five;

    @FXML
    private JFXRadioButton seven;

    @FXML
    private JFXRadioButton ten;

    @FXML
    private JFXRadioButton fifteen;
    @FXML
    private JFXTextField result;

    @FXML
    private Label valueResult;

    @FXML
    private JFXButton translationButton;

    @FXML
    private JFXHamburger menu;

    @FXML
    private AnchorPane settings;

    @FXML
    private JFXSlider firstSlider;

    @FXML
    private JFXSlider secondSlider;

    @FXML
    private JFXSlider thirdSlider;

    @FXML
    private AnchorPane menuTranslation;

    @FXML
    private JFXTextField translateResult;

    private Map<String, Double> hashmap = new HashMap<>();

    private void setHashmap(Map<String, Double> hashmap) {
        hashmap.put("\u20BD", 1.0);
        hashmap.put("€", 73.57);
        hashmap.put("$", 65.4);
        hashmap.put("\u20B8", 0.17);

    }

    @FXML
    void count() {
        if (isCorrect()){
            checkAmount.setPromptText("");
            RadioButton radioButton = (RadioButton) percent.getSelectedToggle();
            result.setText(String.valueOf(Double.parseDouble(radioButton.getText()) * Double.parseDouble(checkAmount.getText()) / 100));
            translation();
        }
    }

    private boolean isCorrect() {
        if (!checkAmount.getText().matches("\\d*\\.?\\d+||\\d+\\.?\\d*")||checkAmount.getText().isEmpty()){
            checkAmount.clear();
            checkAmount.setPromptText("Ошибка!!! Введите другое число");
            return false;
        }
        return true;
    }

    @FXML
    void menuSettings(MouseEvent event) {
        settings.setVisible(!settings.isVisible());
    }

    @FXML
    private void translation() {
        if (!valueBox.getValue().equals("\u20BD")) {
            translateResult.setText(String.valueOf(Double.parseDouble(result.getText()) * hashmap.get(valueBox.getValue())));
        }
    }

    @FXML
    private void translateWindow() {
        menuTranslation.setVisible(true);
        translation();
    }

    public void closeWindow(MouseEvent mouseEvent) {
        window.setVisible(false);
        System.exit(0);
    }

    public void getPercent(ActionEvent actionEvent) {
        percent(0, 51, zero);
        percent(50, 101, three);
        percent(100, 151, five);
        percent(150, 201, seven);
        percent(200, 251, ten);
        percent(250, 301, fifteen);
        count();
    }

    private void percent(int i, int i2, JFXRadioButton three) {
        if (firstSlider.getValue() + secondSlider.getValue() + thirdSlider.getValue() > i && firstSlider.getValue() + secondSlider.getValue() + thirdSlider.getValue() < i2) {
            percent.selectToggle(three);
        }
    }

    @FXML
    void initialize() {
        setHashmap(hashmap);
        ObservableList<String> arr = FXCollections.observableArrayList(hashmap.keySet());
        valueBox.setItems(arr);
        valueBox.setValue("\u20BD");
        valueResult.setText(valueBox.getValue());
        valueBox.setOnAction(event -> valueResult.setText(valueBox.getValue()));
    }
    @FXML
    public void minimizeWindow(MouseEvent mouseEvent) {
        menuTranslation.setVisible(false);
    }
}
