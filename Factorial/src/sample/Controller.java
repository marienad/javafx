package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private Label result;

    @FXML
    private TextField e;

    @FXML
    private Button button;

    //    @FXML
//    void count(ActionEvent event) {
//        double v = Double.parseDouble(e.getText());
//        double a = 1;
//        double f = 0;
//        int i = 1;
//        while (v < i) {
//            a = a * 1 / i;
//            f = f + a;
//            i++;
//        }
//        result.setText(String.valueOf(f));
//    }
    @FXML
    void count(ActionEvent event) {
        double eps = Double.parseDouble(e.getText());
        double a = 1;
        double f = 0;
        int i = 1;
        while (eps < i) {
            a = a * 1 / i;
            f = f + a;
            i++;
        }
        result.setText(String.valueOf(f));
    }
}